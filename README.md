Host metrics collection demonstration written on Python using  BasicHTTPServer, json and psutil libraries, covered by TLS proxy based on Nginx.
Returns some server metrics in Json format via TLS channel.
OS: Debian 9.
